#!/bin/sh

# starting the phoenix server in tmux
# this way we can attach to it later on
tmux new -s phoenix_server -d 'mix deps.get --force && iex -S mix phx.server'
exec tail -f /dev/null