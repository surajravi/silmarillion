defmodule UserInventoryWeb.Schema do
  use Absinthe.Schema

  import_types UserInventoryWeb.Schema.UserTypes

  alias UserInventoryWeb.Resolvers

  def middleware(current_middleware, %{__reference__: %{module: Absinthe.Type.BuiltIns.Introspection}} = _field, _object), do: current_middleware

  def middleware(middleware, field, object) do
    new_middleware = {Absinthe.Middleware.MapGet, to_string(field.identifier)}
    middleware
    |> Absinthe.Schema.replace_default(new_middleware, field, object)
  end

  query do

    @desc "Get all users of Jetsons"
    field :users, list_of(:user) do
      arg :user_uuid, :id
      arg :first_name, :string
      arg :last_name, :string
      arg :email, :string
      arg :per_page, :integer
      arg :current_page, :integer

      resolve &Resolvers.User.filter/3
    end

    @desc "Get a user of Jetsons"
    field :user, :user do
      arg :user_uuid, non_null(:id)

      resolve &Resolvers.User.get/3
    end
  end

  mutation do

    @desc "Create a user"
    field :create_user, :user do
      arg :first_name, non_null(:string)
      arg :last_name, non_null(:string)
      arg :email, non_null(:string)
      arg :password, non_null(:string)

      resolve &Resolvers.User.create/3
    end

    @desc "Update a user"
    field :update_user, :user do
      arg :user_uuid, non_null(:id)
      arg :user, non_null(:user_payload)

      resolve &Resolvers.User.update/3
    end

    @desc "Delete a user"
    field :delete_user, :user do
      arg :user_uuid, non_null(:id)

      resolve &Resolvers.User.delete/3
    end
  end
end
