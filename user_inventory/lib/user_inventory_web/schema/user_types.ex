defmodule UserInventoryWeb.Schema.UserTypes do
  use Absinthe.Schema.Notation

  @desc "A user of Jetsons"
  object :user do
    field :user_uuid, :id
    field :first_name, :string
    field :last_name, :string
    field :email, :string
  end

  @desc "Update parameters for a user of Jetsons"
  input_object :user_payload do
    field :first_name, :string
    field :last_name, :string
    field :email, :string
    field :password, :string
  end

end
