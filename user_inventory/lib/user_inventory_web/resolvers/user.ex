defmodule UserInventoryWeb.Resolvers.User do

  alias UserInventory.User
  @collection "user"

  def create(_parent, args, _resolution) do
    {options, args} = Map.split(args, [:current_page, :per_page])
    {password, args} = Map.pop(args, :password)
    args = Map.merge(args, %{user_uuid: UUID.uuid4(), password_hash: Comeonin.Argon2.hashpwsalt(password)})
    User.create(%{collection: @collection, params: args, options: options})
  end

  def filter(_parent, args, _resolution) do
    {options, args} = Map.split(args, [:current_page, :per_page])
    User.filter(%{collection: @collection, params: args, options: options})
  end

  def get(_parent, %{user_uuid: user_uuid}, _resolution) do
    User.get(%{collection: @collection, params: %{user_uuid: user_uuid}, options: %{}})
  end

  def count(_parent, args, _resolution) do
    {options, args} = Map.split(args, [:current_page, :per_page])
    User.get(%{collection: @collection, params: args, options: options})
  end

  def update(_parent, %{user_uuid: user_uuid, user: args}, _resolution) do
    {options, args} = Map.split(args, [:current_page, :per_page])
    {password, args} = Map.pop(args, :password)
    args = case password do
      nil -> Map.merge(args, %{user_uuid: user_uuid})
      password -> Map.merge(args, %{user_uuid: user_uuid, password_hash: Comeonin.Argon2.hashpwsalt(password)})
    end
    User.update(%{collection: @collection, params: args, options: options})
  end

  def delete(_parent, %{user_uuid: user_uuid}, _resolution) do
    User.delete(%{collection: @collection, params: %{user_uuid: user_uuid}, options: %{}})
  end
end
