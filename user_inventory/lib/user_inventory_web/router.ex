defmodule UserInventoryWeb.Router do
  use UserInventoryWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api" do
    pipe_through :api

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: UserInventoryWeb.Schema,
      interface: :simple,
      json_codec: Phoenix.json_library()

    forward "/", Absinthe.Plug,
      schema: UserInventoryWeb.Schema,
      json_codec: Phoenix.json_library()

  end

end
