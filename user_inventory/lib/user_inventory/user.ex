defmodule UserInventory.User do

  @ascending_sort ~r/up/i
  @descending_sort ~r/down/i
  # the following 'conn' should match the one given to the
  # 'name' key in the mongo db worker in application.ex
  @conn :mongo

  @doc """
  This module's private helper function, which when provided with a options map obtained from the
  user parses it into the following keyworded list and returns the latter

  ### iex example
  iex(1)> extract_option(%{sort: %{name: "up", title: "Down", department: "Up"}, included_fields: [:name, :department], excluded_fields: [:title]})
  [
    skip: 0,
    limit: 0,
    sort: [department: 1, name: 1, title: -1],
    projection: %{_id: 0, department: 1, name: 1, title: 0},
    return_document: :after,
    pool: DBConnection.Poolboy
  ]
  """
  def extract_options(options) when is_map(options) do
    # Set the default pagination counts if none are specified
    page_num = Map.get(options, :current_page, 1)
    page_size = Map.get(options, :per_page, 0)


    # If sort params are specified then properly format them
    sort = Map.get(options, :sort, %{ "_id" => "up" })
           |> Enum.map(fn {key, value} ->
      cond do
        String.match?(value, @ascending_sort) -> {key, 1}
        String.match?(value, @descending_sort) -> {key, -1}
      end
    end)

    # Get included/excluded fields
    # Also, suppress the `_id` field always since we do not want to included that in the data
    # sent back to the client
    included_fields = Map.get(options, :included_fields, [])
    excluded_fields = Map.get(options, :excluded_fields, [])
    excluded_fields = [ :_id | excluded_fields ]

    projection = Map.new(included_fields, fn value -> {value, 1} end)
                 |> Map.merge(Map.new(excluded_fields, fn value -> {value, 0} end))

    [skip: page_size * (page_num - 1), limit: page_size, sort: sort, projection: projection, return_document: :after, pool: DBConnection.Poolboy]
  end

  @doc """
  ### iex example
  iex(1)> UserInventory.User.filter(%{collection: "user", params: %{}, options: %{}})
  {:ok,
  [
   %{
     "email" => "suraj@twoporeguys.com",
     "first_name" => "Suraj",
     "last_name" => "Ravichandran",
     "password_hash" => "4f72ae2a-22ba-4aad-8da8-9e79e8d7ef34",
     "user_uuid" => "8158b32a-4923-49f8-81b0-02f01b005073"
   },
   %{
     "email" => "harry.wintersj@twoporeguys.com",
     "first_name" => "Harry",
     "last_name" => "Winters",
     "password_hash" => "ae1aeb50-a27b-423d-a276-233e9f15d112",
     "user_uuid" => "58309cc9-f6b4-4c3c-bbe3-ff9f7077e2a5"
   }
  ]}
  iex(2)>
  """
  def create(%{collection: collection, params: params, options: options}) do
    case Mongo.insert_one(@conn, collection, params, extract_options(options)) do
      {:ok, inserted_result} -> {:ok, Mongo.find_one(@conn, collection, %{_id: inserted_result.inserted_id}, extract_options(options))}
      some_error -> {:error, "The following error occurred whilst creating #{inspect some_error}"}
    end
  end

  @doc """
  ### iex example
  iex(3)> UserInventory.User.get(%{collection: "user", params: %{user_uuid: "8158b32a-4923-49f8-81b0-02f01b005073"}, options: %{}})
  {:ok,
  %{
   "email" => "suraj@twoporeguys.com",
   "first_name" => "Suraj",
   "last_name" => "Ravichandran",
   "password_hash" => "4f72ae2a-22ba-4aad-8da8-9e79e8d7ef34",
   "user_uuid" => "8158b32a-4923-49f8-81b0-02f01b005073"
  }}
  """
  def filter(%{collection: collection, params: params, options: options}) do
    result = Mongo.find(@conn, collection, params, extract_options(options))
    {:ok, Enum.to_list(result)}
  end

  @doc """
  ### iex example
  iex(3)> UserInventory.User.get(%{collection: "user", params: %{user_uuid: "8158b32a-4923-49f8-81b0-02f01b005073"}, options: %{}})
  {:ok,
  %{
   "email" => "suraj@twoporeguys.com",
   "first_name" => "Suraj",
   "last_name" => "Ravichandran",
   "password_hash" => "4f72ae2a-22ba-4aad-8da8-9e79e8d7ef34",
   "user_uuid" => "8158b32a-4923-49f8-81b0-02f01b005073"
  }}

  iex(4)> UserInventory.User.get(%{collection: "user", params: %{user_uuid: "foobage"}, options: %{}})
  {:error, "User with criteria %{user_uuid: \"foobage\"} not found"}
  """
  def get(%{collection: collection, params: params, options: options}) do
     case Mongo.find_one(@conn, collection, params, extract_options(options)) do
       nil ->
         {:error, "User with criteria #{inspect params} not found"}
       user ->
         {:ok, user}
     end
  end

  @doc """
  ### iex example
  iex(5)> UserInventory.User.count(%{collection: "user", params: %{}, options: %{}})
  {:ok, 2}
  iex(6)> UserInventory.User.count(%{collection: "user", params: %{user_uuid: "8158b32a-4923-49f8-81b0-02f01b005073"}, options: %{}})
  {:ok, 1}
  iex(2)> UserInventory.User.count(%{collection: "user", params: %{user_uuid: "foobage"}, options: %{}})
  {:ok, 0}
  """
  def count(%{collection: collection, params: params, options: options}) do
    case Mongo.count_documents(@conn, collection, params, Keyword.drop(extract_options(options), [:skip, :limit])) do
      {:ok, count_payload} -> {:ok, count_payload}
      {:error, :nothing_returned} -> {:ok, 0}
    end
  end

  @doc """
  iex(3)> UserInventory.User.update(%{collection: "user", params: %{user_uuid: "8158b32a-4923-49f8-81b0-02f01b005073", email: "surajravi@gmail.com"}, options: %{}})
  {:ok,
  %{
   "email" => "surajravi@gmail.com",
   "first_name" => "Suraj",
   "last_name" => "Ravichandran",
   "password_hash" => "4f72ae2a-22ba-4aad-8da8-9e79e8d7ef34",
   "user_uuid" => "8158b32a-4923-49f8-81b0-02f01b005073"
  }}
  iex(6)> UserInventory.User.update(%{collection: "user", params: %{user_uuid: "foobage", email: "surajravi@gmail.com"}, options: %{}})
  {:error,
  "User with criteria %{email: \"surajravi@gmail.com\", user_uuid: \"foobage\"} not found"}
  """
  def update(%{collection: collection, params: params, options: options}) do
    uuid_atom = String.to_atom("#{collection}_uuid")
    case Mongo.find_one_and_update(
           @conn,
           collection,
           %{uuid_atom => Map.get(params, uuid_atom)},
           %{"$set": params},
           extract_options(options)
         ) do
      {:ok, nil} -> {:error, "User with criteria #{inspect params} not found"}
      updated_payload -> updated_payload
    end
  end

  @doc """
  ### iex example
  iex(4)> UserInventory.User.delete(%{collection: "user", params: %{user_uuid: "0c097d52-830b-4374-a5d8-cb6429caaed2"}, options: %{}})
  {:ok,
  %{
   "email" => "surajravi@gmail.com",
   "first_name" => "suraj",
   "last_name" => "lancealot",
   "password_hash" => "47b960c1-997a-4cd6-92fe-d53a2a629879",
   "user_uuid" => "0c097d52-830b-4374-a5d8-cb6429caaed2"
  }}
  iex(5)> UserInventory.User.delete(%{collection: "user", params: %{user_uuid: "foobage"}, options: %{}})
  {:error, "User with criteria %{user_uuid: \"foobage\"} not found"}
  """
  def delete(%{collection: collection, params: params, options: options}) do
    case Mongo.find_one_and_delete(@conn, collection, params, extract_options(options)) do
      {:ok, nil} -> {:error, "User with criteria #{inspect params} not found"}
      deleted_payload -> deleted_payload
    end
  end
end
