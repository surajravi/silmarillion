defmodule UserInventory.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec
    # List all child processes to be supervised
    children = [
      UserInventoryWeb.Endpoint,
      worker(Mongo, [[name: :mongo, url: Application.get_env(:user_inventory, UserInventoryWeb.Endpoint)[:db_url], pool: DBConnection.Poolboy]])
    ]

    opts = [strategy: :one_for_one, name: UserInventory.Supervisor]
    Supervisor.start_link(children, opts)
  end
  def config_change(changed, _new, removed) do
    UserInventoryWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
