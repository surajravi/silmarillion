# UserInventory

To test this MongoDB + Phoenix + Absinthe (GraphQL)

  * Clone this repository
  * cd into `user_inventory` directory
  * Ensure that your local Docker daemon is running,
  then start the dev docker environment `make run`
  * The app is exposed via docker ports at your local laptop's port 4000
  (see instructions below to see the exact urls of interest)
  * _Optional_: To access the elixir interpreter running the phoenix app: `make interactive`]
  This will drop you into the tmux session running the app, do what you wish here, but once
  done do not exit tmux session, just detach via `Ctrl + b` and then exit from the shell via `exit`

For the GraphQL `/graphql` visit [`localhost:4000/api`](http://localhost:4000/api) from your browser.
For the GraphiQL `/graphiql` visit [`localhost:4000/api/graphiql`](http://localhost:4000/api/graphiql) from your browser.

## Graphql queries and mutation examples

First visit the GraphiQL endpoint

Then try on the following queries and mutations

### Filter and get examples
```
query filter_users{
  users {
    userUuid
    firstName
    lastName
    email
  }
}
```

```
query filter_users{
  users(firstName: "Suraj") {
    userUuid
    firstName
    lastName
    email
  }
}
```

```
query filter_users{
  users(firstName: "Suraj", userUuid: "1d559acd-825e-4ee9-94e3-786d34de4eec") {
    userUuid
    firstName
    lastName
    email
  }
}
```

```
query filter_users{
  users(perPage: 20, currentPage: 2) {
    userUuid
    firstName
    lastName
    email
  }
}
```

```
query get_user{
  user(userUuid: "1d559acd-825e-4ee9-94e3-786d34de4eec") {
    userUuid
    firstName
    email
  }
}
```


### Mutations for creating, updating, and deleting users

```
mutation create_user {
  createUser(firstName: "Peter", lastName: "Mercado", email: "peter.mercado@gmail.com", password: "changeme") {
    userUuid
  }
}

```

```
mutation update_user {
  updateUser(userUuid: "0614f387-e174-432a-80c9-66b4e9eea5f1", user: {email: "peterpan@gmail.com", password: "foobage"}) {
    firstName
    lastName
    email
  }
}
```


```
mutation delete_user {
  deleteUser(userUuid: "1d559acd-825e-4ee9-94e3-786d34de4eec") {
    userUuid
    firstName
    lastName
  }
}
```