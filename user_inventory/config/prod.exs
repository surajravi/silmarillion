use Mix.Config

config :user_inventory, UserInventoryWeb.Endpoint,
  cache_static_manifest: "priv/static/cache_manifest.json",
  secret_key_base: "Hm4gNKn23XClQb01bIT5LiUfmGNToiHV3YPrSKxgxo+04uJOCilrArlvrwQO9W1R",
  server: true

config :logger, level: :info

