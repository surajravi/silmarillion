use Mix.Config

config :user_inventory, UserInventoryWeb.Endpoint,
  load_from_system_env: true,
  http: [port: System.get_env("PORT") |> String.to_integer()],
  url: [host: "localhost", port: System.get_env("PORT") |> String.to_integer()],
  db_url: System.get_env("DB_URL"),
  secret_key_base: "MGnLs9/DAcREB3VFU3PjtZD4s4cCMDht6khhpdhlSwwy3UWHBNIVa/N3gJY/N/C9",
  render_errors: [view: UserInventoryWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: UserInventory.PubSub, adapter: Phoenix.PubSub.PG2]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :phoenix, :json_library, Jason


import_config "#{Mix.env()}.exs"
