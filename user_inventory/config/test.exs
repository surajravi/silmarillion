use Mix.Config

config :user_inventory, UserInventoryWeb.Endpoint,
  http: [port: 4002],
  server: false

config :logger, level: :warn
