defmodule Vacillate do
  @moduledoc """
  Documentation for Vacillate.
  """
  @kafka_client Application.get_env(:vacillate, :kafka_client)

  @doc """
  Hello world.

  ## Examples

      iex> Vacillate.hello()
      :world

  """
  def hello do
    :world
  end


end
