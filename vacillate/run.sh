#!/bin/sh
# this is the dev docker container's initial start script

echo "Installing project dependencies... please wait"
mix deps.get
# starting the iex session in tmux
# this way we can attach to it later on
tmux new -s vacillate_interpreter -d 'iex -S mix'
exec tail -f /dev/null

