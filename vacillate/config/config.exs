use Mix.Config

config :kafka_ex,
       brokers: System.get_env("KAFKA_BROKERS") || "localhost:9092",
       disable_default_worker: true

config :vacillate,
       is_test: false,
       kafka_client: KafkaEx

try do
  import_config "#{Mix.env()}.exs"
rescue
  _ -> :ok
end
