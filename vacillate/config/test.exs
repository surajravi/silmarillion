use Mix.Config

config :vacillate,
       is_test: true,
       disable_kafka_connection: !!(System.get_env("INTEGRATION_TEST") || false),
       kafka_client: if !!(System.get_env("INTEGRATION_TEST") || false), do: KafkaExMock, else: KafkaEx
