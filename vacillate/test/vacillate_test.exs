defmodule VacillateTest do
  use ExUnit.Case
  @kafka_client Application.get_env(:vacillate, :kafka_client)

  test "greets the world" do
    assert Vacillate.hello() == :world
  end
end
