defmodule Vacillate.MixProject do
  use Mix.Project

  def project do
    [
      app: :vacillate,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Vacillate.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:kafka_ex, "~> 0.10"},
      {:snappy, git: "https://github.com/fdmanana/snappy-erlang-nif"},
      {:charlatan, git: "https://gitlab.ontera.bio/sw-team/charlatan.git", only: :test}
    ]
  end
end
